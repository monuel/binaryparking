﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.


namespace CoolParking.BL.Services
{
    using CoolParking.BL.Models;
    using Interfaces;
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    class ParkingService : IParkingService
    {
        public void AddVehicle(Vehicle vehicle)
        {
            try
            {
                vehicles.Add(vehicle);
            }catch(InvalidOperationException ex)
            {
                throw ex;
            }
        }

        public void Dispose()
        {
            throw new System.NotImplementedException();
        }

        public decimal GetBalance()
        {
            return Balance;
        }

        public int GetCapacity()
        {
            return vehicles.Capacity;
        }

        public int GetFreePlaces()
        {
            return vehicles.Capacity - vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            var vehiclesCollection = new ReadOnlyCollection<Vehicle>(vehicles);
            return vehiclesCollection;
        }

        public string ReadFromLog()
        {
            throw new System.NotImplementedException();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicleToRemove = vehicles.Where(t => t.Id == vehicleId);
            vehicles.Remove(vehicleToRemove.FirstOrDefault());
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            throw new System.NotImplementedException();
        }
        public ParkingService()
        {
            vehicles = new List<Vehicle>();
            transactions = new List<TransactionInfo>();
        }
        public int Balance { get; internal set; }
        List<Vehicle> vehicles;
        List<TransactionInfo> transactions;
    }
}