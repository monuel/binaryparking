﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.


namespace CoolParking.BL.Models
{
    using System;
    using System.Text;
    using System.Text.RegularExpressions;
    class Vehicle
    {
        string id = null;
        private VehicleType vehicleType;
        decimal balance = 0;

        public string Id { get { return id; } }
        public VehicleType VehicleType { get { return vehicleType; } }
        public decimal Balance { get; internal set; }

        public Vehicle(VehicleType vehicleType, decimal balance)
        {
            this.id = GenerateRandomRegistrationPlateNumber();
            this.vehicleType = vehicleType;
            this.balance = balance;
        }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (Regex.IsMatch(Id, "^[A-Z]+[-]+[0-9]+[-]+[A-Z]+$"))
            {
                this.id = id;
                this.vehicleType = vehicleType;
                this.balance = balance;
            }
        }

        static char GenerateRandomLetter()
        {
            Random random = new Random();

            char letter;

            double flt = random.NextDouble();
            int shift = Convert.ToInt32(Math.Floor(25 * flt));
            letter = Convert.ToChar(shift + 65);

            return letter;
        }

        static int GenerateRandomNumber()
        {
            return new Random().Next(0, 9);
        }

        static string GenerateRandomRegistrationPlateNumber()
        {
            StringBuilder newIdentifier = new StringBuilder();

            for (int i = 0; i < 8; i++)
            {
                if (i < 2 || i > 5)
                {
                    newIdentifier.Append(GenerateRandomLetter());
                }
                else if (i == 2 || i == 5)
                {
                    newIdentifier.Append('-');
                    newIdentifier.Append(GenerateRandomNumber().ToString());
                }
                else if (i > 2 || i < 5)
                {
                    newIdentifier.Append(GenerateRandomNumber());
                }
            }
            return newIdentifier.ToString();
        }
    }
}