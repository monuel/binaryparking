﻿// TODO: implement enum VehicleType.
//       Items: PassengerCar, Truck, Bus, Motorcycle.


namespace CoolParking.BL.Models
{
    enum VehicleType{
        PassengerCar,
        Truck,
        Bus,
        Motorcycle
    }
}